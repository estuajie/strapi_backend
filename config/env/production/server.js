module.exports = ({ env }) => ({
  url: env("URL"),
  host: env('HOST'),
  port: env.int('PORT'),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '3ad3200f53c1331344c76d57636cb103'),
    },
  },
});

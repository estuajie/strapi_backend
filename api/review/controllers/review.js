'use strict';

const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

module.exports = { 
  async findMe(ctx) {
    let entities;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    entities = await strapi.query('review').find({ user: user.id })

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.review }));
  },
  async createMe(ctx) {
    let entity;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data['user'] = user;
      entity = await strapi.services.review.create(data, { files });
    } else {
      const data = ctx.request.body;
      data['user'] = user;
      entity = await strapi.services.review.create(data);
    }
    return sanitizeEntity(entity, { model: strapi.models.review });
  },

//   async updateMe(ctx) { 
//     let entity;
//     const user = ctx.state.user;
//     if(!user){
//         return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
//     }
//     if (ctx.is('multipart')) {
//       const { data, files } = parseMultipartData(ctx);
//       data['user'] = user;
//       entity = await strapi.services.review.update({ user: user.id }, data, {
//         files,
//       });
//     } else {
//       const data = ctx.request.body;
//       data['user'] = user;
//       entity = await strapi.services.review.update({ user: user.id }, data);
//     }

//     return sanitizeEntity(entity, { model: strapi.models.review });
//   },
//   async deleteMe(ctx) {
//     const user = ctx.state.user;
//     if(!user){
//         return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
//     } 
        
//     const entity = await strapi.services.review.delete({ user: user.id });
//     // here review files have been deleted by services.review.delete    
  
//     return sanitizeEntity(entity, { model: strapi.models.review });
//   }
};
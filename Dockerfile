# FROM node:14-alpine as BUILD_IMAGE
FROM strapi/base as BUILD_IMAGE

WORKDIR /srv/app

# Resolve node_modules for caching
COPY ./package.json ./
COPY ./yarn.lock ./
COPY ./.env ./
RUN yarn install --production=true
RUN rm -rf node_modules/sharp
RUN yarn add --arch=x64 --platform=linux sharp

# Copy all for build and release cache if package.json update
COPY . .

# ENV NODE_ENV=production 

ENV NODE_ENV production
ENV ENV_PATH /app/.env

ENV HOST 0.0.0.0
ENV PORT 1337
ENV DATABASE_CLIENT mysql
ENV DATABASE_HOST 143.198.202.216
ENV DATABASE_PORT 3306
ENV DATABASE_NAME campDB
ENV DATABASE_USERNAME camp_user
ENV DATABASE_PASSWORD manis123
ENV DATABASE_SSL false

RUN yarn build

#------------------------------------------------------------------------------------

# Create new namespace for final Docker Image
FROM strapi/base

# Only copy your source code without system file
COPY --from=BUILD_IMAGE /srv/app /srv/app

WORKDIR /srv/app

EXPOSE 1337

ENV NODE_ENV=production
ENV STRAPI_LOG_LEVEL=debug

CMD ["yarn", "start"]
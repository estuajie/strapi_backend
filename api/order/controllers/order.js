'use strict';

const { parseMultipartData, sanitizeEntity } = require('strapi-utils'); 
const x = require('../../../config/env/production/xendit');
const tokenXenditCallback = 'jjwyTVmVlq7SJ14vvzfikxGHuskgPGIs8k2EwnBIFVMTBBEP'

const { Balance } = x;
const b = new Balance({});

const { Invoice } = x;
const i = new Invoice({});

const VirtualAcc = x.VirtualAcc;
const va = new VirtualAcc({});

module.exports = { 
  async findMe(ctx) {
    let entities;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    entities = await strapi.query('order').find({ user: user.id })

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.order }));
  }, 
 
  async createMe(ctx) {  
    const dataUrl = ctx.request.body;
    // console.log("dataUrl", dataUrl) 
    let entities;
    entities = await strapi.query('order').find({ invoice_number: dataUrl.invoice_number }) 
    // console.log("entitiesentities", entities)
    const dataID = entities.map(x => x.id)
    // console.log("dataID", dataID)
    // const dataID = dataUrl.invoice_number
    const dataChange = {  invoice_url: dataUrl.invoice_url } 
    await strapi.services.order.update({  id: dataID  }, dataChange);  

    return `Data invoice_url : ${dataUrl.invoice_number} Berhasil di Simpan`  
  },

  async updateMe(ctx) { 
    let entity;
    const user = ctx.state.user;
    console.log("user", user)
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    } else { 
        const data = ctx.request.body;
        console.log("data", data)
        let entities;
        entities = await strapi.query('order').find({ invoice_number: data.review_order.inv_order }) 
        // data['user'] = user;
        const dataID = entities.map(x => x.id)
        console.log("dataID", dataID)
        // await strapi.services.order.update({  id: dataID  }, dataChange);  
        await strapi.services.order.update({ id: dataID }, data); 
        return `Data Berhasil di Simpan` 
        // return sanitizeEntity(entity, { model: strapi.models.order });
    } 
  },

  async createInvoice(ctx) {  
    let entity;
    const user = ctx.state.user;
    if(!user){
        return ctx.request(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data['user'] = user;
      entity = await strapi.services.order.create(data, { files });
    } else {
      // create Xendit Invoice Payment
      try {
        const data = ctx.request.body;  
        if (data.category === 'perlengkapan') {
            const data = ctx.request.body;    
            const itemsDataMap = data.items.map(index => {
              return {
                name: index.name,
                price: index.price,
                quantity: index.quantity
              }
            }) 
            const feesDataMap = data.fees.map(index => {
              return {
                type: index.type,
                value: index.value
              }
            })   
            
            let invoice = await i.createInvoice({ 
              // externalID: 'Invoice_' + Date.now().toString(),
              externalID: data.invoice_number,
              payerEmail: data.info_user_order.payer_email,
              description: data.info_user_order.description,
              amount: data.info_user_order.amount, 
              customer: {
                given_names: data.info_user_order.given_names,
                email: data.info_user_order.email, 
                mobile_number: data.info_user_order.mobile_number,  
                addresses: [
                  {
                    country: data.pengiriman.country,
                    street_line1: data.pengiriman.street_line1, 
                    city: data.pengiriman.city,
                    province: data.pengiriman.province,
                    state: data.pengiriman.state, 
                  }
                ]
              },
              items: itemsDataMap,
              fees: feesDataMap,
              customerNotificationPreference: {
                invoice_created: ['email'],
              } 
            });  

            const retrievedInvoice = await i.getInvoice({ invoiceID: invoice.id }); 
            if(retrievedInvoice){ 
                const data = ctx.request.body;
                data['user'] = user;   
                entity = await strapi.services.order.create(data); 
            }
            // Untuk Kirim Email 
            const sendTo = data.info_user_order.payer_email
            strapi.log.debug(`Trying to send an email to ${sendTo}`)
            try {
              const emailOptions = {
                to: sendTo,
                subject: 'Pesanan Anda Telah Kami Terima',
                html: `<h1>Terima Kasih, Pesanan Anda Telah Kami Terima!</h1><br>
                      <p>Invoice Number: ${data.invoice_number}</p> 
                      <p>Nomor Handphone: ${data.info_user_order.mobile_number}</p>  
                      <p>Deskripsi: ${data.info_user_order.description}</p> 
                      <p>Total Pembarayan: ${data.amount}</p> 
                      <p>Pembarayan Anda telah kami terima, kami akan segera memproses pemesanan Anda.</p>
                      <p>Terima Kasih</p>
                      `,
              }
              await strapi.plugins['email'].services.email.send(emailOptions)
              strapi.log.debug(`Email sent to ${sendTo}`)
              // ctx.send({ message: 'Email sent' })
            } catch (err) {
              strapi.log.error(`Error sending email to ${sendTo}`, err)
              ctx.send({ error: 'Error sending email' })
            }
            return sanitizeEntity(retrievedInvoice, { model: strapi.models.order }); 
        } else {
            const data = ctx.request.body;    
            const itemsDataMap = data.items.map(index => {
              return {
                name: index.name,
                price: index.price,
                quantity: index.quantity
              }
            }) 
            const feesDataMap = data.fees.map(index => {
              return {
                type: index.type,
                value: index.value
              }
            })   
            
            let invoice = await i.createInvoice({ 
              // externalID: 'Invoice_' + Date.now().toString(),
              externalID: data.invoice_number,
              payerEmail: data.info_user_order.payer_email,
              description: data.info_user_order.description,
              amount: data.info_user_order.amount, 
              customer: {
                given_names: data.info_user_order.given_names,
                email: data.info_user_order.email, 
                mobile_number: data.info_user_order.mobile_number,  
                addresses: [
                  {
                    country: "IDN",
                    street_line1: "JLN", 
                    city: "JKT",
                    province: "DKI",
                    state: "JAWA", 
                  }
                ]
              },
              items: itemsDataMap,
              fees: feesDataMap,
              customerNotificationPreference: {
                invoice_created: ['email'],
              } 
            });  

            const retrievedInvoice = await i.getInvoice({ invoiceID: invoice.id }); 
            if(retrievedInvoice){ 
                const data = ctx.request.body;
                data['user'] = user;   
                entity = await strapi.services.order.create(data); 
            }
            // Untuk Kirim Email 
            const sendTo = data.info_user_order.payer_email
            strapi.log.debug(`Trying to send an email to ${sendTo}`)
            try {
              const emailOptions = {
                to: sendTo,
                subject: 'Pesanan Anda Telah Kami Terima',
                html: `<h1>Terima Kasih, Pesanan Anda Telah Kami Terima!</h1><br>
                      <p>Invoice Number: ${data.invoice_number}</p> 
                      <p>Nomor Handphone: ${data.info_user_order.mobile_number}</p>  
                      <p>Deskripsi: ${data.info_user_order.description}</p> 
                      <p>Total Pembarayan: ${data.amount}</p> 
                      <p>Pembarayan Anda telah kami terima, kami akan segera memproses pemesanan Anda.</p>
                      <p>Terima Kasih</p>
                      `,
              }
              await strapi.plugins['email'].services.email.send(emailOptions)
              strapi.log.debug(`Email sent to ${sendTo}`)
              // ctx.send({ message: 'Email sent' })
            } catch (err) {
              strapi.log.error(`Error sending email to ${sendTo}`, err)
              ctx.send({ error: 'Error sending email' })
            }
            return sanitizeEntity(retrievedInvoice, { model: strapi.models.order }); 
        }
      } catch (e) {
        console.error(e); // eslint-disable-line no-console 
        statusCode: 400
        return sanitizeEntity(e, { model: strapi.models.order }); 
      }
    }
    return sanitizeEntity(entity, { model: strapi.models.order });

  }, 

  async handleCallbackXendit(ctx) { 
    const headerSender = ctx.request.header 
    console.log("headerSender :", headerSender)
    if(headerSender === tokenXenditCallback) {
      console.log("TRUE")
    } else console.log("FALSE")

    const dataCallback = ctx.request.body;
    console.log("dataCallback", dataCallback) 
    let entities;
    entities = await strapi.query('order').find({ invoice_number: dataCallback.external_id }) 
    const dataID = entities.map(x => x.id)
    if(dataCallback.status === 'EXPIRED') {
      const dataChange = { 
        status_payment: dataCallback.status,  
      } 
      await strapi.services.order.update({  id: dataID  }, dataChange);   
      return `Data ${dataCallback.external_id} dengan status ${dataCallback.status} Berhasil di Simpan` 
    } if(dataCallback.status === 'PAID') {
      const dataChange = { 
          status_payment: dataCallback.status, 
          xendit_response_information: {
            xendit_id: dataCallback.id,
            xendit_user_id: dataCallback.user_id,
            is_high: dataCallback.is_high,
            payment_method: dataCallback.payment_method,
            paid_amount: dataCallback.paid_amount,
            fees_paid_amount: dataCallback.fees_paid_amount,
            adjusted_received_amount: dataCallback.adjusted_received_amount,
            bank_code: dataCallback.bank_code,
            initial_amount: dataCallback.initial_amount,
            paid_at: dataCallback.paid_at,
            payment_channel: dataCallback.payment_channel,
            payment_destination: dataCallback.payment_destination,
          } 
        }  
        await strapi.services.order.update({  id: dataID  }, dataChange);   
        // Untuk Kirim Email
        let entities;
        entities = await strapi.query('order').find({ invoice_number: dataCallback.external_id })  
        const sendTo = entities[0].info_user_order.email
        const info = entities[0]
        strapi.log.debug(`Trying to send an email to ${sendTo}`)
        try {
          const emailOptions = {
            to: sendTo,
            subject: 'Pembayaran Berhasil',
            html: `<h1>Selamat, Pembayaran Anda Berhasil!</h1><br>
                  <p>Invoice Number: ${info.invoice_number}</p>
                  <p>Tanggal Pemesanan: ${info.tanggal_pemesanan}</p>
                  <p>Nomor Handphone: ${info.info_user_order.mobile_number}</p>
                  <p>Nama Pemesan: ${info.info_user_order.given_names}</p><br>
                  <p>Total Pembayaran: Rp. ${info.xendit_response_information.adjusted_received_amount}</p>
                  <p>Metode Pembayaran: ${info.xendit_response_information.payment_method} - ${info.xendit_response_information.payment_channel}</p>
                  <p>Pembarayan Anda telah kami terima, kami akan segera memproses pemesanan Anda.</p>
                  <p>Terima Kasih</p>
                  `,
          }
          await strapi.plugins['email'].services.email.send(emailOptions)
          strapi.log.debug(`Email sent to ${sendTo}`)
          ctx.send({ message: 'Email sent' })
        } catch (err) {
          strapi.log.error(`Error sending email to ${sendTo}`, err)
          ctx.send({ error: 'Error sending email' })
        }
        return `Data ${dataCallback.external_id} dengan status ${dataCallback.status} Berhasil di Simpan` 
    } 

  },

  async getBalance(ctx) {
    try {
      const r = await b.getBalance({
        accountType: Balance.AccountType.Holding,
      });
      console.log('Holding balance NOW:', r); // eslint-disable-line no-console
      return sanitizeEntity(r, { model: strapi.models.order }); 
    } catch (e) {
      console.error(e); // eslint-disable-line no-console 
      return sanitizeEntity(e, { model: strapi.models.order }); 
    }  
  }, 
  
  async getVA(ctx) {
    try {
      const banks = await va.getVABanks();
      console.log('available va banks:', banks); // eslint-disable-line no-console
  
      const fixedAcc = await va.createFixedVA({
        externalID: '123',
        bankCode: banks[0].code,
        name: 'Stanley Nguyen',
      });
      // eslint-disable-next-line no-console
      console.log('fixed va created:', fixedAcc);
  
      const { id } = fixedAcc;
      const retrievedAcc = await va.getFixedVA({ id });
      // eslint-disable-next-line no-console
      console.log('fixed va details:', retrievedAcc);
  
      const updatedAcc = await va.updateFixedVA({
        id,
        suggestedAmt: 20,
        expectedAmt: 30,
      });
      // eslint-disable-next-line no-console
      console.log('updated va details:', updatedAcc); 

    } catch (e) {
      console.error(e); // eslint-disable-line no-console 
    }
  },
};
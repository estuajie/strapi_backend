module.exports = ({ env }) => ({
    //...
    meilisearch: {
      // Your meili host
      host: env('SEARCH_URL'),
      // Your master key
      api_key: env('SEARCH_MASTER_KEY'),
    },

    email: {
      provider: 'mailgun',
      providerOptions: {
        apiKey: env('MAILGUN_API_KEY'),
        domain: env('MAILGUN_DOMAIN'), //Required if you have an account with multiple domains
        // host: env('MAILGUN_HOST', 'api.mailgun.net'), //Optional. If domain region is Europe use 'api.eu.mailgun.net'
      },
      settings: {
        defaultFrom: env('MAILGUN_DEFAULT_FROM_MAIL'),
        defaultReplyTo: env('MAILGUN_DEFAULT_REPLY_MAIL'),
      },
    },
    upload: {
      provider: 'cloudinary',
      providerOptions: { 
        cloud_name: env('CLOUDINARY_NAME'),
        api_key: env('CLOUDINARY_API_KEY'),
        api_secret: env('CLOUDINARY_API_SECRET'),
      },
    },    
    //...
  })
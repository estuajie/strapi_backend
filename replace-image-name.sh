#!/bin/sh

# Only works on Linux (add sed -i -> '' etc for working on Mac)

search={IMAGE_NAME}
replace=./replace-image-name.sh registry.gitlab.com/your/strapi-image:somelonglonglongsha
escaped_replace=$(echo $replace | sed -e 's/[\/&]/\\&/g')

sed -i "s/${search}/${escaped_replace}/g" docker-compose.yml
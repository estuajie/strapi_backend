'use strict';

const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

module.exports = { 
  async findMe(ctx) {
    let entities;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    entities = await strapi.query('profile').find({ user: user.id })

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.profile }));
  },
  async createMe(ctx) {
    let entity;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data['user'] = user;
      entity = await strapi.services.profile.create(data, { files });
    } else {
      const data = ctx.request.body;
      data['user'] = user;
      entity = await strapi.services.profile.create(data);
    }
    return sanitizeEntity(entity, { model: strapi.models.profile });
  },

  async updateMe(ctx) { 
    let entity;
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    }
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data['user'] = user;
      entity = await strapi.services.profile.update({ user: user.id }, data, {
        files,
      });
    } else {
      const data = ctx.request.body;
      data['user'] = user;
      entity = await strapi.services.profile.update({ user: user.id }, data);
    }

    return sanitizeEntity(entity, { model: strapi.models.profile });
  },
  async deleteMe(ctx) {
    const user = ctx.state.user;
    if(!user){
        return ctx.badRequest(null, [{message: [{ id: 'No auth header found' }]}]);
    } 
        
    const entity = await strapi.services.profile.delete({ user: user.id });
    // here profile files have been deleted by services.profile.delete    
  
    return sanitizeEntity(entity, { model: strapi.models.profile });
  }
};
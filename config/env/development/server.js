module.exports = ({ env }) => ({
  url: "http://localhost:1337/",
  host: "localhost",
  port: 1337,
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '3ad3200f53c1331344c76d57636cb103'),
    },
  },
});
